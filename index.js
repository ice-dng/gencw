var prompt = require('prompt');

dashboard_template = {
    "widgets": [
        {
            "type": "metric",
            "x": 0,
            "y": 0,
            "width": 6,
            "height": 6,
            "properties": {
                "view": "timeSeries",
                "stacked": false,
                "metrics": [
                    [ "AWS/EC2", "CPUUtilization", "AutoScalingGroupName", "<AUTOSCALINGGROUPNAME>" ],
                    [ ".", "CPUCreditBalance", ".", "." ],
                    [ ".", "CPUCreditUsage", ".", "." ],
                    [ "AWS/EC2", "CPUUtilization", "AutoScalingGroupName", "AUTOSCALINGGROUPNAMEFORWEBRTC" ],
                    [ ".", "CPUCreditBalance", ".", "." ],
                    [ ".", "CPUCreditUsage", ".", "." ]
                ],
                "region": "us-east-2"
            }
        },
        {
            "type": "metric",
            "x": 6,
            "y": 0,
            "width": 6,
            "height": 6,
            "properties": {
                "view": "timeSeries",
                "stacked": false,
                "metrics": [
                    [ "AWS/S3", "BucketSizeBytes", "StorageType", "StandardStorage", "BucketName", "<S3 BUCKET NAME>", { "period": 86400 } ],
                    [ "...", ",<S3 BUCKET NAME>-logs", { "period": 86400 } ]
                ],
                "region": "us-east-2"
            }
        },
        {
            "type": "metric",
            "x": 12,
            "y": 0,
            "width": 9,
            "height": 6,
            "properties": {
                "view": "timeSeries",
                "stacked": false,
                "metrics": [
                    [ "ICEAPI", "RequestCount", "stack", "<STACK-NAME>" ],
                    [ ".", "ActiveThreadCount", ".", "." ],
                    [ ".", "RequestTimeAverage", ".", "." ],
                    [ ".", "ExceptionCount", ".", "." ],
                    [ ".", "RequestTimeTotal", ".", "." ],
                    [ ".", "AvailHttpThreadPercentage", ".", "." ],
                    [ ".", "JVMFreeMemoryMB", ".", "." ]
                ],
                "region": "us-east-2"
            }
        },
        {
            "type": "metric",
            "x": 0,
            "y": 6,
            "width": 12,
            "height": 9,
            "properties": {
                "view": "timeSeries",
                "stacked": false,
                "metrics": [
                    [ "AWS/ELB", "Latency", "LoadBalancerName", "<STACK-NAME>-elb", "AvailabilityZone", "us-east-2a" ],
                    [ "...", "us-east-2b" ],
                    [ ".", "SurgeQueueLength", ".", ".", ".", "us-east-2a" ],
                    [ "...", "us-east-2b" ],
                    [ ".", "RequestCount", ".", ".", ".", ".", { "stat": "Sum" } ],
                    [ "...", "us-east-2a", { "stat": "Sum" } ],
                    [ ".", "Latency", ".", "umich-training-elb-WebRTC", ".", "." ],
                    [ ".", "SurgeQueueLength", ".", ".", ".", "." ],
                    [ ".", "BackendConnectionErrors", ".", ".", ".", "." ],
                    [ ".", "HealthyHostCount", ".", ".", ".", "." ]
                ],
                "region": "us-east-2"
            }
        },
        {
            "type": "metric",
            "x": 12,
            "y": 6,
            "width": 9,
            "height": 9,
            "properties": {
                "view": "timeSeries",
                "stacked": false,
                "metrics": [
                    [ "AWS/RDS", "CPUUtilization", "DBInstanceIdentifier", "<STACK-NAME>" ],
                    [ ".", "DatabaseConnections", ".", "." ],
                    [ ".", "ReadIOPS", ".", "." ],
                    [ ".", "WriteIOPS", ".", "." ]
                ],
                "region": "us-east-2"
            }
        }
    ]
}

var main = function() {
  prompt.start();

  prompt.get(['Region','AutoScalingGroupName','WebRTCAutoScalingGroupName', 'BucketName', 'StackName'], function (err, result) {
    //
    // Log the results.
    //
    //Regions
    dashboard_template.widgets[0].properties.region = result.Region;
    dashboard_template.widgets[1].properties.region = result.Region;
    dashboard_template.widgets[2].properties.region = result.Region;
    dashboard_template.widgets[3].properties.region = result.Region;
    dashboard_template.widgets[4].properties.region = result.Region;

    //AutoScalingGroups
    dashboard_template.widgets[0].properties.metrics[0][3] = result.AutoScalingGroupName;
    dashboard_template.widgets[0].properties.metrics[3][3] = result.WebRTCAutoScalingGroupName;
    //s3
    dashboard_template.widgets[1].properties.metrics[0][5] = result.BucketName;
    dashboard_template.widgets[1].properties.metrics[1][1] = result.BucketName+"-logs";
    //ICEAPI
    dashboard_template.widgets[2].properties.metrics[0][3] = result.StackName;
    //ELB
    dashboard_template.widgets[3].properties.metrics[0][3] = result.StackName + "-elb";
    dashboard_template.widgets[3].properties.metrics[0][5] = result.Region + "a";
    dashboard_template.widgets[3].properties.metrics[1][1] = result.Region + "b";
    dashboard_template.widgets[3].properties.metrics[2][5] = result.Region + "a";
    dashboard_template.widgets[3].properties.metrics[3][1] = result.Region + "b";
    dashboard_template.widgets[3].properties.metrics[5][1] = result.Region + "a";
    dashboard_template.widgets[3].properties.metrics[6][3] = result.StackName + "-elb-WebRTC";

    //RDS
    dashboard_template.widgets[4].properties.metrics[0][3] = result.StackName;

    console.log(JSON.stringify(dashboard_template));

  });


}
main()
